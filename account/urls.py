from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    # попередній перегляд входу
    path('login/', auth_views.LoginView.as_view(template_name='registration/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='registration/login.html'), name='logout'),
    #  path('logout/', auth_views.LogoutView.as_view(template_name='account/logged_out.html'), name='logout'),
    # зміни URL-адрес пароля
    path('password_change/', auth_views.PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    # скидання паролів urls
    path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    # альтернативний спосіб включення представлень автентифікації
    # path('', include('django.contrib.auth.urls')),
    path('register/', views.register, name='register'),
]
